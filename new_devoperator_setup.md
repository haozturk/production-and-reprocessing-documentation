# CERN Computing Account

# E-group subscriptions

Every P&R dev-operator needs to be a member of the following [egroups](https://e-groups.cern.ch/e-groups/EgroupsSearch.do):

1. cms-comp-ops-workflow-team
2. cms-comp-ops
3. cms-comp-ops-toolsandint
4. cms-service-unified

### Admin egroups:

P&R admins should also be a member of the following admin egroups

1. cms-comp-ops-workflow-team-admin
2. cms-service-unified-admins


# CERN Grid Certificate & VOMS Proxy Setup

1. If you don't yet have one, get your [grid certificate](https://ca.cern.ch/ca/) by selecting "New Grid User certificate."

2. Create *.pem files for public-key authentication. On your local computer:
```
cd ~ # this moves to your home area 
openssl pkcs12 -in myCert.p12 -out myPublicCert.pem -clcerts -nokeys # this creates: myPublicCert.pem 
openssl pkcs12 -in myCert.p12 -out myPrivKey.pem -nocerts #this creates : myPrivKey.pem
``` 
If you don't have OpenSSL installed on your computer, you may perform steps 3 and 4 to send the certificate (the *.p12 file) to lxplus and perform step 2 afterwards. For more information, see the appropriate [twiki](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookStartingGrid#ObtainingCert).

3. Go to lxplus. For information on how to connect to lxplus, see the appropriate [twiki](https://twiki.cern.ch/twiki/bin/view/LCGAAWorkbook/LoggingIn).

4. Move *pem files to the appropriate directory (typically <code>.globus/</code>)

On lxplus:
```
mkdir .globus [this is the standard place for voms certificates]
```

On your local machine, send your *.pem files to lxplus:
```
cd ~
scp ~/myPrivKey.pem [username]@lxplus.cern.ch:.globus/userkey.pem
scp ~/myPublicCert.pem [username]@lxplus.cern.ch:.globus/usercert.pem
```

On lxplus: 
```
cd .globus
chmod 400 userkey.pem   # owner read only
chmod 600 usercert.pem   # owner R&W
```


# CMS VO Registration:

1. Go the following page and follow the instructions: https://voms2.cern.ch:8443/voms/cms/user/home.action 


2. Initiate your voms proxy and test if it works

```
voms-proxy-init -voms cms 
```
If everything went fine, you should get an output similar to the following one
```
Contacting voms.cern.ch:15002 [/DC=ch/DC=cern/OU=computers/CN=voms.cern.ch] "cms"...
Remote VOMS server contacted succesfully.
Created proxy in /tmp/x509up_uXXXX.
Your proxy is valid until Fri May 24 21:53:28 CEST 2019
```

# WTC/ACDC Console Registration

WTC/ACDC console is reachable only within CERN network. In order to access it from outside, a proxy tool needs to be used.

We suggest to use FoxyProxy: https://addons.mozilla.org/en-US/firefox/addon/foxyproxy-standard/

Install this extension to your browser. Set 

- `SOCKS5` as your Proxy type
- `localhost` as your proxy ip address
- `8090` as your port

And then open up your terminal and run
```
ssh -ND 8090 [username]@lxplus.cern.ch
```

Now you should be able to access to the website.

1. Register using the following link: https://wfrecovery.cern.ch/newuser 

2. Try to submit an ACDC [Reference to the ACDC Console documentation]

# Unified

Existing Unified machines are:

- vocms0268
- vocms0269
- vocms0272
- vocms0273
- vocms0274
- vocms0275
- vocms0277

You can access to these machines from lxplus if you're a member of `cms-service-unified` egroup

### JIRA bypass permission

In order to bypass a workflow from JIRA, you should be in this list: 

https://github.com/CMSCompOps/WmAgentScripts/blob/c24a1f72121357d3c9225c869d05b7ae985042d9/unifiedConfiguration.json#L131-L143 

Please put yourself in this list and create a pull request


