# WMAgentScripts/Unified

[Unified](https://github.com/CMSCompOps/WmAgentScripts) is a set of scripts which have critical roles in production. They run as cronjobs.

Currently, we're in the process of migrating Unified into python3. In 2021, we started to re-write Unified in a much maintainable manner in python3 and deployed a few scripts in production. However, since it's quite urgent to move to python-3 and it takes a lot of time to improve the code while migrating into python3, we decided to migrate the remaining scripts to python3 w/o any improvement and resume the improvement phase once everything starts running in python3. Therefore, we have 3 versions of Unified running in production right now which are explained below.

## Schedule & Acrontab settings:

All listed scripts, their schedule and the machine that they can be seen below

```
$ acrontab -l
##sync the code from master
*/10 * * * * vocms0268 /data/unified/WmAgentScripts/sync.sh &> /dev/null
*/10 * * * * vocms0269 /data/unified/WmAgentScripts/sync.sh &> /dev/null
*/10 * * * * vocms0272 /data/unified/WmAgentScripts/sync.sh &> /dev/null
###*/10 * * * * vocms0273 /data/unified/WmAgentScripts/sync.sh &> /dev/null
## sync the code from master
4 */2 * * * vocms0269 /data/unified/WmAgentScripts/credentials.sh create &> /dev/null
6 */1 * * * vocms0268 /data/unified/WmAgentScripts/credentials.sh create &> /dev/null
8 */1 * * * vocms0272 /data/unified/WmAgentScripts/credentials.sh create &> /dev/null
###9 */2 * * * vocms0273 /data/unified/WmAgentScripts/credentials.sh create &> /dev/null
12 */2 * * * vocms0275 /data/unified/WmAgentScripts/credentials.sh create &> /dev/null
## sync 0275 .json to master
*/40 * * * * vocms0275 /data/unified/WmAgentScripts/cWrap.sh Unified/pushor.py &> /dev/null
*/5 * * * * vocms0275 source /data/unified/WmAgentScripts/whatjob.sh > /eos/cms/store/unified/www/info.txt 2> /dev/null
## single script that cycles through modules
00,30 * 1-31 * * vocms0269 /data/unified/WmAgentScripts/assigncycle.sh &> /dev/null
10,40 * 1-31 * * vocms0269 /data/unified/WmAgentScripts/postcycle-strict.sh &> /dev/null
15,45 * 1-31 * * vocms0269 /data/unified/WmAgentScripts/postcycle-update.sh &> /dev/null
20,50 * 1-31 * * vocms0268 /data/unified/WmAgentScripts/postcycle-review-recovering.sh &> /dev/null
25,55 * 1-31 * * vocms0269 /data/unified/WmAgentScripts/postcycle-review-manual.sh &> /dev/null
*/5 * 1-31 * * vocms0269 /data/unified/WmAgentScripts/actcycle.sh &> /dev/null
5,35 * 1-31 * * vocms0268 /data/unified/WmAgentScripts/clearcycle.sh  &> /dev/null
#25,55 * 1-31 * * vocms0268 /data/unified/WmAgentScripts/shortcycle.sh &> /dev/null
##*/10 * 1-31 * * vocms0268 /data/unified/WmAgentScripts/viewcycle.sh &> /dev/null
*/15 * 1-31 * * vocms0275 /data/unified/WmAgentScripts/errorcycle.sh &> /dev/null
*/25 * 1-31 * * vocms0275 /data/unified/WmAgentScripts/runerrorcycle.sh &> /dev/null
*/15 */2 1-31 * * vocms0275 /data/unified/WmAgentScripts/toperrorcycle.sh &> /dev/null
25,55 * 1-31 * * vocms0268 /data/unified/WmAgentScripts/jiraCycle.sh &> /dev/null
## announce stuff
00 10 * * 1 vocms0269 /data/unified/WmAgentScripts/cWrap.sh Unified/messagor.py
## Monitoring
*/5 * * * * vocms0268 /afs/cern.ch/user/c/cmsunified/mon/unified_component_mon.sh > /dev/null
*/5 * * * * lxplus /afs/cern.ch/user/c/cmsunified/mon/unified_status_mon.sh > /dev/null
## Cleanup
05 00 * * 1 vocms0268 /data/unified/WmAgentScripts/cleanlog.sh 
10 00 * * 1 vocms0269 /data/unified/WmAgentScripts/cleanlog.sh 
15 00 * * 1 vocms0272 /data/unified/WmAgentScripts/cleanlog.sh 
###20 00 * * 1 vocms0273 /data/unified/WmAgentScripts/cleanlog.sh 
30 00 * * 1 vocms0275 /data/unified/WmAgentScripts/cleanlog.sh 
00 11 * * 1 vocms0275 /data/unified/WmAgentScripts/cleanBackfills.sh &> /dev/null
## Python3 Production
15,45 * 1-31 * * vocms0277 /data/unifiedPy3/WmAgentScripts/src/bash/injectorCycle.sh &> /dev/null
00 * 1-31 * * vocms0277 /data/unifiedPy3/WmAgentScripts/src/bash/invalidatorCycle.sh &> /dev/null
```

## 1. Python2 Unified

This version is kept in [the master branch](https://github.com/CMSCompOps/WmAgentScripts)

Scripts are running under our service account `cmsunified`.

### Environment

- `credentials.sh` and `set.sh` scripts handle the necessary credential and dependency settings.
- `sync.sh` fetches and merges the latest changes from the integration machine's master branch

### Script descriptions

Unified scripts running in this version are listed below:

#### Assignor

Responsible for workflow assignment: `assignment-approved` to `assigned` transition

#### Batchor

RelVal workflows run in batches. This script creates campaign entries for each RelVal batch. 

#### Checkor

Checks the workflows in various statuses. Responsible for 

- `completed` to `closed-out` transition
- `assistance-*` labeling

and some additional checks.


#### ShowError

Responsible to construct the error reports

#### Actor

Responsible to get ACDC requests from wtc console and submit them into ReqMgr

#### Closor

Responsible for 

- `closed-out` to `announced` transition
- setting output datasets `VALID` in DBS.


#### Completor 

This script has lots of outdated functionalities which should be removed. One useful functionality is to automatically do updates on JIRA tickets which should be migrated into `JiraHandler`

#### JiraHandler

Closes JIRA tickets of archived/inactive workflows


### Logs

Unified logs can be found at `/eos/cms/store/unified/www/logs` which is accessible from all unified machines (check the acrontab to see the machines)


## 2. Python3 Improved Unified

This version is kept in [the python3-migration branch](https://github.com/CMSCompOps/WmAgentScripts/tree/python3-migration)

Scripts are running under our service account `cmsunified`

### Environment

`/data/unifiedPy3/setUnifiedPy3Env.sh` sets up the environment. This script is not in git at the moment.

### Script descriptions

Unified scripts running in this version are listed below:

#### Injector & Rejector

Responsible for

- Injecting newly injected workflows which are in `assignment-approved` state into the Unified database. If Injector doesn't inject a workflow into its database, that workflow is invisible to all other unified modules. It does some checks before injecting a workflow into the database such as duplicate check, input validity check etc.
- Stepchain conversion: Every MC request is injected as taskchain into the system. It's up to Injector & Rejector to assess the eligibility of the workflow for the conversion and perform the conversion for eligible requests.

#### Invalidator

When PDMV wants to reject a workflow and/or invalidate some datasets, they publish such datasets in a certain endpoint of McM. Unified queries this endpoint periodically and performs the rejections/invalidations. 

### Logs

Unified logs for this version can be found at `/eos/cms/store/unified/www/py3logs` which is accessible from all unified machines (check the acrontab to see the machines)



## 3. Python3 Not-improved Unified

This version is kept in [the python3-migration-fast branch](https://github.com/CMSCompOps/WmAgentScripts/tree/python3-migration-fast)

to be documented



