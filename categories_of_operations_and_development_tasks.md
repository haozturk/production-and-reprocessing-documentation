# Categories of operations and development duties

## Monitoring production
   1. [Job monitoring and Resource utilization](https://monit-grafana.cern.ch/d/000000628/cms-job-monitoring-es-agg-data-official?orgId=11&refresh=15m&var-group_by=Site&var-Tier=All&var-CMS_WMTool=All&var-CMS_SubmissionTool=All&var-CMS_CampaignType=All&var-Site=All&var-Type=production&var-CMS_JobType=All&var-CMSPrimaryDataTier=All&var-binning=1h&from=now-30d&to=now-1h)

   We should make sure that we're utilizing our resources to the most possible extent

   2. [Failure rate checking](https://monit-grafana.cern.ch/d/u_qOeVqZk/wmarchive-monit?from=now-12h&orgId=11&to=now&var-campaign=All&var-jobtype=LogCollect&var-jobtype=Merge&var-jobtype=Processing&var-jobtype=Production&var-host=All&var-jobstate=All&var-exitCodes=All)

   We should check if there is a workflow whose failure rate or a site where failure rate is high.

   3. [Request type distribution and CPU Efficiency](https://monit-grafana.cern.ch/d/HbdOtARnk/user-hozturk-pnr-ops?orgId=11)

   *Request type distribution:* We should check the workflow distribution by request type and see what's taking what portion of production. Having too many taskchain jobs is dangerous due to its disadvantages on request turnaround time and storage usage.

   4. [WMAgent Monitoring](https://monit-grafana.cern.ch/d/lhVKAhNik/cms-wmagent-monitoring?orgId=11&refresh=5m)

   [Number of workflows in a given status](https://monit-grafana.cern.ch/d/lhVKAhNik/cms-wmagent-monitoring?orgId=11&refresh=5m&viewPanel=1062) is the most important plot in this dashboard. We should make sure that workflow transitions are working properly. Secondly, [stuck workflows page](https://dmytro.web.cern.ch/dmytro/cmsprodmon/stuck_workflows.php) is also very important to see how many workflows are stalled at which state.

   Moreover, [Agent Current Status](https://monit-grafana.cern.ch/d/lhVKAhNik/cms-wmagent-monitoring?orgId=11&refresh=5m&viewPanel=492) is a useful plot to see active agents and their statuses.

   5. [WMStats](https://cmsweb.cern.ch/wmstats/index.html): Active workflow and job monitoring

   WMStats is super useful to filter active workflows by sitewhitelist, input dataset etc. and see their details. Moreover, it's also possible to do historical search over archived workflows using the `search` tab.
   
   6. Running/Pending jobs per agent/schedd:

   To check which agent manages how many jobs, please check [Running jobs per schedd](https://monit-grafana.cern.ch/d/lEihqUfmz/cms-submission-infrastructure-schedd-view?orgId=11&viewPanel=10) and [Pending jobs per schedd](https://monit-grafana.cern.ch/d/lEihqUfmz/cms-submission-infrastructure-schedd-view?orgId=11&viewPanel=11)

## Workflows in Assistance Status
## Stuck Workflow Queue Elements (WQE)
## Delayed Release Validations (relvals)
## Computing Site Issues
## Usage of Opportunistic Computing Resources
## Stuck Workflows
## Miscellaneous 
